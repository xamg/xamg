# XAMG
**XAMG** is a library for solving sparse systems of linear algebraic equations with multiple right-hand side vectors. The library focuses on multiple solutions of systems with constant matrices and contains optimized methods implementation for the solution phase only.

The library contains the implementation of:

- a set of BiCGStab methods, including the merged formulations [1];
- algebraic multigrid methods (the matrix hierarchy is built using the **hypre** library);
- Jacobi and Gauss-Seidel-like methods;
- Chebyshev polynomials.

[1] B. Krasnopolsky, Revisiting Performance of BiCGStab Methods for Solving Systems with Multiple Right-Hand Sides // Computers and Mathematics With Applications, 79(9), 2020, [doi](http://dx.doi.org/10.1016/j.camwa.2019.11.025) [arXiv:1907.12874](https://arxiv.org/abs/1907.12874)

The following features are implemented in the **XAMG** library [2]:
- mixed precision floating point calculations [3];
- dynamic indices compression (for 1 or 2-byte integers);
- per-level multigrid parameters specification.

[2] B. Krasnopolsky, A. Medvedev, XAMG: A library for solving linear systems with multiple right-hand side vectors // SoftwareX, 14, 2021, [doi](http://dx.doi.org/10.1016/j.softx.2021.100695)

[3] B. Krasnopolsky, A. Medvedev, Evaluating Performance of Mixed Precision Linear Solvers with Iterative Refinement // Supercomputing Frontiers and Innovations, 2021 (in press)

**{TODO:}** The following features are planned to be implemented in the code:
- native support for 64-bit integer indices;
- adaptive data representation using various matrix storage formats for each matrix block.

The code is implemented in C++ (based on C++11 standard), which allows on par with data alignment to provide vectorization for all the main computational blocks. The current version of the code supports hybrid three-level hierarchical parallel programming model (MPI+POSIX shared memory). Optional use of GPU accelerators (multiGPU mode) is planned to be implemented in the future.

The library provides API for usage with applications written in C. The Fortran API is planned to be developed in the future.

# Building and running an example on a new system

Very basic build scheme that is supposed to work on every generic system:

1) `git clone --recursive XAMG...`
> *NOTE: It is important to add option `--recursve` to automatically clone dependent repos*
2) Choose right compilers (manually tuning PATH or using 'module', it depends)
3) `cd ThirdParty` && `<set CC,CXX,MPICC,MPICXX variables to show right compilers>` && `./dnb.sh` && `cd ..`
4) `cd examples/cpp`
5) `make CONFIG=generic`

For more sofisticated build procedure instructions and to have some insight into setting optimal
build flags for a specific HPC system please consult the XAMG library build reference
(see the URL in the `Documentation` section below).

# Makefile options:
`make [<target> <target> ...] [variable=value variable=value ...]`

## Usage examples:
```
make clean cpp example BUILD=Release
make BUILD=Debug XAMG_USER_FLAGS="-DXAMG_NV=2"
make WITH_CUDA=TRUE
make CONFIG=generic CXX=icpc MPICXX=/opt/openmpi-1.5.0/bin/mpicxx
```
## Variables for make:
- `BUILD=Release|Debug`  (default is: `Debug`)
- `XAMG_USER_FLAGS="<any_compiler_flags>"`  (default is: `<empty>`)
- `WITH_SEPARATE_OBJECTS=TRUE|FALSE` (default is: `FALSE`)
- `WITH_LIMITED_TYPES_USAGE=TRUE|FALSE` (default is: `TRUE`)
- `WITH_CUDA=TRUE|FALSE` (default is: `FALSE`)
- `WITH_GCOV=TRUE|FALSE` (default is: `FALSE`) NOT IMPLEMENTED YET!
- `WITH_GPROF=TRUE|FALSE` (default is: `FALSE`)
- `WITH_CUDA_PROFILE=TRUE|FALSE` (default is: `FALSE`) NOT IMPLEMENTED YET!
- `WITH_ITAC=TRUE|FALSE` (default is: `FALSE`)
- `CONFIG=<config-file-name>.inc` (default is: `<username>-<hostname>.inc` placed either in current dir or `Make.inc/`)
- `MACHINEID=<machineid>`   (default is: `<empty>`. Is supposed to be set in config. Can be GENERIC to pick up generic options.
> *NOTE: the `machineid` string is just a suffix for some variables group in compflags.inc. One can introduce any new machineid.*
- `CXX=<compiler>` (default: supposed to be set in config.inc, otherwise is replaced with `c++`)
- `CC=<compiler>` (default: supposed to be set in config.inc, otherwise is replaced with `cc`)
- `MPICXX=<compiler>` (default: supposed to be set in config.inc, otherwise is replaced with `mpicxx`)
- `MPICC=<compiler>` (default: supposed to be set in config.inc, otherwise is replaced with `mpicc`)
- `NV_CXX=<compiler>` (default: supposed to be set in config.inc, otherwise is replaced with `nvcc`)

## Documentation

[Comprehensive build documentation](https://gitlab.com/xamg/xamg/-/wikis/docs/XAMG_build_guideline)

[List of numerical methods' parameters](https://gitlab.com/xamg/xamg/-/wikis/docs/XAMG_params_reference)

## Acknowledgements

The code development was supported by the Russian Science Foundation (RSF) Grant No. 18-71-10075.
