/****************************************************************************
** 
**  Copyright (C) 2019-2021 Boris Krasnopolsky, Alexey Medvedev
**  Contact: xamg-test@imec.msu.ru
** 
**  This file is part of the XAMG library.
** 
**  Commercial License Usage
**  Licensees holding valid commercial XAMG licenses may use this file in
**  accordance with the terms of commercial license agreement.
**  The license terms and conditions are subject to mutual agreement
**  between Licensee and XAMG library authors signed by both parties
**  in a written form.
** 
**  GNU General Public License Usage
**  Alternatively, this file may be used under the terms of the GNU
**  General Public License, either version 3 of the License, or (at your
**  option) any later version. The license is as published by the Free 
**  Software Foundation and appearing in the file LICENSE.GPL3 included in
**  the packaging of this file. Please review the following information to
**  ensure the GNU General Public License requirements will be met:
**  https://www.gnu.org/licenses/gpl-3.0.html.
** 
****************************************************************************/

#include <sstream>
#include <iostream>
#include <fstream>
#include <regex>
#include <argsparser.h>

#include <xamg/xamg_headers.h>
#include <xamg/xamg_types.h>

#include <xamg/init.h>

#include <xamg/blas/blas.h>
#include <xamg/blas2/blas2.h>
#include <xamg/solvers/solver.h>

#include <xamg/sys/dbg_helper.h>

#include "cmdline.h"

///////////////////

extern ID id;
extern XAMG::time_monitor monitor;
extern XAMG::perf_info perf;

///////////////////

#ifndef XAMG_NV
#define XAMG_NV 16
#endif

#ifndef FP_TYPE
#define FP_TYPE float64_t
#endif

#ifdef ITAC_TRACE
#include <VT.h>
#endif

const uint16_t NV = XAMG_NV;

#include "../common/system/system.h"
#include "tst_output.h"
#include "tst_blas.h"
#include "tst_spmv.h"
#include "tst_solver.h"
#include "tst_hypre.h"

int main(int argc, char *argv[]) {
    std::vector<std::string> solver_roles = {"meta_solver",  "solver",        "preconditioner",
                                             "pre_smoother", "post_smoother", "coarse_grid_solver"};
    args_parser parser(argc, argv, "-", ' ');
    dbg_helper dbg;
    dbg.add_parser_options(parser);
    execution_mode_t execution_mode;
    XAMG::params::global_param_list params;
    auto res = parse_cmdline(parser, solver_roles, execution_mode, params);
    switch (res) {
    case PARSE_OK:
        break;
    case PARSE_FATAL_FAILURE:
        return 1;
    case PARSE_HELP_PRINTED:
        return 0;
    }
    sleep(parser.get<int>("sleep"));
    params.set_defaults();
    dbg.get_parser_options();
    dbg.start(argv[0]);
    std::vector<std::string> logfile;
    parser.get<std::string>("logfile", logfile);
    if (logfile.size() == 1)
        logfile.push_back("");
    XAMG::init(argc, argv, parser.get<std::string>("node_config"), logfile[0], logfile[1]);
    dbg.init_output(id.gl_proc);
    tst_store_output tst_output;
    tst_output.init();
#ifdef ITAC_TRACE
    VT_traceoff();
#endif

    XAMG::matrix::matrix m(XAMG::mem::DISTRIBUTED);
    XAMG::vector::vector x(XAMG::mem::DISTRIBUTED), b(XAMG::mem::DISTRIBUTED);
    if (execution_mode != blas_test) {
        auto mtx = make_system<FP_TYPE>(parser, m, x, b, parser.get<bool>("graph_reordering"),
                                        parser.get<bool>("save_pattern"));
        if (!b.if_initialized)
            XAMG::blas::set_const<FP_TYPE, NV>(b, 1.0);
        //if (!b.if_initialized)
        //    XAMG::blas::set_rand<FP_TYPE, NV>(b, false);
        if (!x.if_initialized)
            XAMG::blas::set_const<FP_TYPE, NV>(x, 0.0);
        tst_output.store_item("info", "matrix", mtx);
    }

    uint64_t niters = parser.get<int>("test_iters");
    if (execution_mode == blas_test) { // blas functions
        XAMG::vector::vector xx(XAMG::mem::DISTRIBUTED), yy(XAMG::mem::DISTRIBUTED),
            zz(XAMG::mem::DISTRIBUTED);
        niters = (niters ? niters : 1000);
        tst_blas_test<FP_TYPE>(xx, yy, zz, parser, niters, tst_output);
    }
    if (execution_mode == solver_test) { // XAMG solver
        niters = (niters ? niters : 3);
        tst_solver_test<FP_TYPE>(m, x, b, params, niters, tst_output);
    }
    if (execution_mode == spmv_test) { // spmv
        niters = (niters ? niters : 3);
        XAMG::blas::set_rand<FP_TYPE, NV>(x, false);
        tst_spmv_test<FP_TYPE>(m, x, b, niters, tst_output);
    }
    if (execution_mode == hypre_test) { // hypre solver
        niters = (niters ? niters : 3);
        tst_hypre_test(m, x, b, params, niters, tst_output);
    }

    XAMG::finalize();
    if (id.master_process()) {
        auto result_filename = parser.get<std::string>("result");
        if (parser.get<std::string>("load") != "") {
            tst_output.store_item("info", "config", parser.get<std::string>("load"));
        }
        tst_output.dump(result_filename);
    }
}
